import 'dart:collection';
import 'dart:convert';
import 'package:bancamovilapp/modelo/credito.dart';
import 'package:bancamovilapp/modelo/cuenta.dart';
import 'package:bancamovilapp/modelo/mensaje.dart';
import 'package:bancamovilapp/modelo/transferencia.dart';
import 'package:bancamovilapp/modelo/usuario.dart';
import 'package:http/http.dart' as http;

import 'Global.dart';

String urlUsu =
    "http://" + localhost + "/ProjectTransfer/ws/servicios/transferencia";

transferenciaPost(Transferencia u) async {
  String url = urlUsu;
  Map data = {
    "cuentaOrigen": '${u.numeroOrigen}',
    "cuentaDestino": '${u.numeroDestino}',
    "monto": u.monto
  };
  //encode Map to JSON
  var dat = json.encode(data);

  Map<String, String> headers = new HashMap();
  headers.putIfAbsent('Accept', () => 'application/json');

  var response = await http.post(url,
      headers: {"Content-Type": "application/json"}, body: dat);

  print('Response status: ${response.statusCode}');
  print('Response body: ${response.body}');
  Mensaje m = Mensaje.fromJson(json.decode(response.body));
  print(m.id);
  if (m.id == 200) {
    refrescar();
    print(usuarioGlobal.saldoCuenta);
  } else {}
}

String urlLogin =
    "http://" + localhost + "/ProjectTransfer/ws/servicios/usuario?email=";

Future<bool> usuarioLogin(String correo, String password) async {
  String url = urlLogin + correo + "&pass=" + password;
  Map<String, String> headers = new HashMap();
  headers.putIfAbsent('Accept', () => 'application/json');
  http.Response response = await http.get(
    url,
    headers: headers,
  );
  print('Response status: ${response.statusCode}');
  print('Response body: ${response.body}');
  Usuario j = Usuario.fromJson(json.decode(response.body));

  if (j.codigo == 200) {
    print("entra");
    usuarioGlobal = j;
    return true;
  } else {
    print("No entra");
    return false;
  }
}

String urlUsuarioLog =
    "http://" + localhost + "/ProjectTransfer/ws/servicios/usuario?email=";
Future<Usuario> fetchPost(String correo, String pass) async {
  urlUsuarioLog = urlUsuarioLog + correo + "&pass=" + pass;
  var response = await http.get(urlUsuarioLog);
  Usuario j = Usuario.fromJson(json.decode(response.body));
  print('Response body2: ${response.body}');
  if (j.codigo == 200) {
    // Si el servidor devuelve una repuesta OK, parseamos el JSON

    return Usuario.fromJson(json.decode(response.body));
  } else {
    // Si esta respuesta no fue OK, lanza un error.
    throw Exception('Failed to load post');
  }
}

refrescar() async {
  String url =
      urlLogin + usuarioGlobal.correo + "&pass=" + usuarioGlobal.password;
  Map<String, String> headers = new HashMap();
  headers.putIfAbsent('Accept', () => 'application/json');
  http.Response response = await http.get(
    url,
    headers: headers,
  );
  print('Response status: ${response.statusCode}');
  print('Response body: ${response.body}');
  Usuario j = Usuario.fromJson(json.decode(response.body));
  usuarioGlobal = j;
}

resetPaswword(String correo) async {
  String url = "http://" + localhost + "/ProjectTransfer/ws/servicios/password";
  Map data = {"cedula": '${usuarioGlobal.cedula}', "correo": correo};
  //encode Map to JSON
  var dat = json.encode(data);

  Map<String, String> headers = new HashMap();
  headers.putIfAbsent('Accept', () => 'application/json');

  var response = await http.post(url,
      headers: {"Content-Type": "application/json"}, body: dat);

  print('Response status: ${response.statusCode}');
  print('Response body: ${response.body}');
  Mensaje m = Mensaje.fromJson(json.decode(response.body));
  print(m.id);
  if (m.id == 200) {
    print("ok");
    print(usuarioGlobal.saldoCuenta);
    reset = true;
  } else {
    print("no ok");
    reset = false;
  }
}

getCreditos() async {
  String url = "http://" +
      localhost +
      "/ProjectTransfer/ws/servicios/credito?cuenta=" +
      usuarioGlobal.numCuenta;
  Map<String, String> headers = new HashMap();
  headers.putIfAbsent('Accept', () => 'application/json');

  http.Response response = await http.get(
    url,
    headers: headers,
  );
  print('Response status: ${response.statusCode}');
  print('Response body: ${response.body}');
  creditos = (json.decode(response.body) as List)
      .map((data) => Credito.fromJson(data))
      .toList();
  for (var name in creditos) {
    print(name.proposito);
    for (var amor in name.listaAmortizacion) {
      print(amor.estadoAmortizacion);
    }
  }
  //Credito c = Credito.fromJson(json.decode(response.body));
  //print(c.monto);
}

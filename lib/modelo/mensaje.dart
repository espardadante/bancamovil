class Mensaje {
  final int id;
  final String mensaje;

  Mensaje({this.id, this.mensaje});

  factory Mensaje.fromJson(Map<String, dynamic> json) {
    return Mensaje(
      id: json['id'],
      mensaje: json['mensaje'],
    );
  }
}

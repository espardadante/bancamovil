class Transferencia {
  final String numeroOrigen;
  final String numeroDestino;
  final double monto;

  Transferencia(this.numeroOrigen, this.numeroDestino, this.monto);
  
}

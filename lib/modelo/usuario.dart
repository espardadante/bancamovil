class Usuario {
  final int codigo;
  final String cedula;
  final String numCuenta;
  final double saldoCuenta;
  final String nombre;
  final String apellido;
  final String tipo;
  final String correo;
  final String password;

  Usuario({
    this.codigo,
    this.cedula,
    this.numCuenta,
    this.saldoCuenta,
    this.nombre,
    this.apellido,
    this.tipo,
    this.correo,
    this.password,
  });

  factory Usuario.fromJson(Map<String, dynamic> json) {
    return Usuario(
      codigo: json['codigo'],
      cedula: json['cedula'],
      numCuenta: json['numCuenta'],
      saldoCuenta: json['saldoCuenta'],
      nombre: json['nombre'],
      apellido: json['apellido'],
      tipo: json['tipo'],
      correo: json['correo'],
      password: json['password'],
    );
  }
}

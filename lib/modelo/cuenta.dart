class Cuenta {
  final String numCuenta;
  final String tipo;
  final double saldoCuenta;

  Cuenta(this.numCuenta, this.tipo, this.saldoCuenta);
  
}

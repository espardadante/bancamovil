class Credito {
  final int idCredito;
  final List<Amortizacion> listaAmortizacion;
  final double monto;
  final String proposito;
  final double saldo;

  Credito(
      {this.idCredito,
      this.listaAmortizacion,
      this.monto,
      this.proposito,
      this.saldo});
  factory Credito.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['listaAmortizacion'] as List;
    print(list.runtimeType);
    List<Amortizacion> pagosList =
        list.map((i) => Amortizacion.fromJson(i)).toList();

    return Credito(
      idCredito: parsedJson['idCredito'],
      listaAmortizacion: pagosList,
      monto: parsedJson['monto'],
      proposito: parsedJson['proposito'],
      saldo: parsedJson['saldo'],
    );
  }
}

class Amortizacion {
  final int idAmortizacion;
  final int periodoAmortizacion;
  final String fechaCuota;
  final String estadoAmortizacion;
  final double cuotaAmortizacion;
  final double capitalPendienteAmortizacion;

  Amortizacion(
      {this.idAmortizacion,
      this.periodoAmortizacion,
      this.fechaCuota,
      this.estadoAmortizacion,
      this.cuotaAmortizacion,
      this.capitalPendienteAmortizacion});
  factory Amortizacion.fromJson(Map<String, dynamic> parsedJson) {
    return Amortizacion(
      idAmortizacion: parsedJson['idAmortizacion'],
      periodoAmortizacion: parsedJson['periodoAmortizacion'],
      fechaCuota: parsedJson['fechaCuota'],
      estadoAmortizacion: parsedJson['estadoAmortizacion'],
      cuotaAmortizacion: parsedJson['cuotaAmortizacion'],
      capitalPendienteAmortizacion: parsedJson['capitalPendienteAmortizacion'],
    );
  }
}

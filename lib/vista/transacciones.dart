import 'package:bancamovilapp/modelo/cuenta.dart';
import 'package:bancamovilapp/modelo/transferencia.dart';
import 'package:bancamovilapp/servicio/Global.dart';
import 'package:bancamovilapp/servicio/UsuarioService.dart';
import 'package:flutter/material.dart';

class TransactionPageState extends State<ThirdRoute> {
  GlobalKey<FormState> _key2 = GlobalKey();
  TextEditingController cuentaOrigen =
      new TextEditingController(text: usuarioGlobal.numCuenta);
  TextEditingController cuentaDestino = new TextEditingController();
  TextEditingController monto = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Transferencias"),
      ),
      body: new SingleChildScrollView(
        child: new Container(
          margin: new EdgeInsets.all(25.0),
          child: new Form(
            key: _key2,
            child: formUI(),
          ),
        ),
      ),
    );
  }

  formItemsDesign(item) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 3),
      child: Card(child: ListTile(title: item)),
    );
  }

  Widget formUI() {
    return Column(
      children: <Widget>[
        formItemsDesign(TextFormField(
          controller: cuentaOrigen,
          decoration: new InputDecoration(
            labelText: 'Numero de Cuenta Origen:',
          ),
        )),
        formItemsDesign(TextFormField(
          controller: cuentaDestino,
          decoration: new InputDecoration(
            labelText: 'Numero de Cuenta Destino:',
          ),
        )),
        formItemsDesign(TextFormField(
          controller: monto,
          decoration: new InputDecoration(
            labelText: 'Monto:',
          ),
        )),
        GestureDetector(
            onTap: () {
              setState(() {
                save();
              });

              Navigator.pop(context);
            },
            child: Container(
              margin: new EdgeInsets.all(30.0),
              alignment: Alignment.center,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                gradient: LinearGradient(colors: [
                  Color(0xFF0EDED2),
                  Color(0xFF03A0FE),
                ], begin: Alignment.topLeft, end: Alignment.bottomRight),
              ),
              child: Text("Guardar",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w500)),
              padding: EdgeInsets.only(top: 10, bottom: 10),
            ))
      ],
    );
  }

  save() {
    Transferencia c = new Transferencia(
        cuentaOrigen.text, cuentaDestino.text, double.parse(monto.text));
    transferenciaPost(c);
  }
}

class ThirdRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TransactionPageState();
  }
}

import 'package:bancamovilapp/modelo/credito.dart';
import 'package:bancamovilapp/servicio/Global.dart';
import 'package:bancamovilapp/servicio/UsuarioService.dart';
import 'package:bancamovilapp/vista/detalleCredito.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainCreditoState extends State<FifthRoute> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Creditos"),
      ),
      body: DataTable(
          columns: const <DataColumn>[
            DataColumn(
              label: Text(
                "#",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                "Tipo",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                "Valor",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                "Saldo",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                "Fecha Vencimiento",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(label: Text("detalle")),
          ],
          rows: creditos
              .map((element) => DataRow(cells: <DataCell>[
                    DataCell(Text(element.idCredito.toString())),
                    DataCell(Text(element.proposito)),
                    DataCell(Text(element.monto.toString())),
                    DataCell(Text(element.saldo.toString())),
                    DataCell(Text(element.proposito)),
                    DataCell(FlatButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        disabledColor: Colors.grey,
                        disabledTextColor: Colors.black,
                        padding: EdgeInsets.all(8.0),
                        splashColor: Colors.blueAccent,
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DetailScreen(
                                      todo: element.listaAmortizacion)));
                          print(element.idCredito.toString());
                        },
                        child: Text("detalle")))
                  ]))
              .toList()),
    );
  }
}

class FifthRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainCreditoState();
  }
}

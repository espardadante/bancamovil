import 'package:bancamovilapp/servicio/Global.dart';
import 'package:bancamovilapp/servicio/UsuarioService.dart';
import 'package:bancamovilapp/vista/detalleCuenta.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  static Route<dynamic> route() {
    return MaterialPageRoute(
      builder: (context) => LoginScreen(),
    );
  }

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  GlobalKey<FormState> _key = GlobalKey();

  RegExp emailRegExp =
      new RegExp(r'^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$');
  RegExp contRegExp = new RegExp(r'^([1-zA-Z0-1@.\s]{1,255})$');
  String _correo;
  String _contrasena;
  String mensaje = '';

  initState() {
    super.initState();
    controller = AnimationController(
      duration: const Duration(milliseconds: 1000),
      vsync: this,
    );
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);

    //    Descomentar las siguientes lineas para generar un efecto de "respiracion"
    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        controller.forward();
      }
    });

    controller.forward();
  }

  dispose() {
    // Es importante SIEMPRE realizar el dispose del controller.
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: logueado
          ? SecondRoute() /*HomeScreen(mensaje: mensaje) */
          : loginForm(),
//      body: loginForm(),
    );
  }

  Widget loginForm() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 150,
              width: 150,
              child: Image.network(
                  "https://img.icons8.com/cotton/128/000000/user-male--v1.png"),
            ),
          ],
        ),
        Container(
          width: 350.0, //size.width * .6,
          margin: EdgeInsets.only(top: 10.0),

          child: Form(
            key: _key,
            child: Column(
              children: <Widget>[
                headerSection(),
                TextFormField(
                  validator: (text) {
                    if (text.length == 0) {
                      return "Este campo correo es requerido";
                    } else if (!emailRegExp.hasMatch(text)) {
                      return "El formato para correo no es correcto";
                    }
                    return null;
                  },
                  keyboardType: TextInputType.emailAddress,
                  maxLength: 50,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    //hintText: 'Correo',
                    labelText: 'Correo',
                    counterText: '',
                    icon: Icon(Icons.email, size: 32.0, color: Colors.black),
                  ),
                  onSaved: (text) => _correo = text,
                ),
                TextFormField(
                  obscureText: true,
                  validator: (text) {
                    if (text.length == 0) {
                      return "Este campo contraseña es requerido";
                    } else if (text.length <= 0) {
                      return "Su contraseña debe ser al menos de 1 caracteres";
                    } else if (!contRegExp.hasMatch(text)) {
                      return "El formato para contraseña no es correcto";
                    }
                    return null;
                  },
                  keyboardType: TextInputType.text,
                  maxLength: 20,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    //hintText: 'Contraseña',
                    labelText: 'Contraseña',
                    counterText: '',
                    icon: Icon(Icons.lock, size: 32.0, color: Colors.black),
                  ),
                  onSaved: (text) => _contrasena = text,
                ),
                new SizedBox(
                  height: 45.0,
                  child: new Center(
                    child: new Container(
                      margin: new EdgeInsetsDirectional.only(
                          start: 15.0, end: 15.0),
                      height: 9.5,
                      color: Colors.white10,
                    ),
                  ),
                ),
                GestureDetector(
                    onTap: () async {
                      if (_key.currentState.validate()) {
                        _key.currentState.save();
                        //Aqui se llamaria a su API para hacer el login
                        setState(() {
                          usuarioLogin(_correo, _contrasena).then((onValue) {
                            logueado = onValue;
                          });
                        });

//                      Una forma correcta de llamar a otra pantalla
                        //     Navigator.of(context).push(ListViewDetalle.route(mensaje));
                      }
                    },
                    child: Container(
                      margin: new EdgeInsets.all(1.0),
                      alignment: Alignment.center,
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        gradient: LinearGradient(
                            colors: [
                              Color(0xFF01579B),
                              Color(0xFF03A0FE),
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight),
                      ),
                      child: Text("Login",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w500)),
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                    )),

                new SizedBox(
                  height: 20.0,
                  child: new Center(
                    child: new Container(
                      margin: new EdgeInsetsDirectional.only(
                          start: 15.0, end: 15.0),
                      height: 9.5,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Container headerSection() {
    return Container(
      margin: EdgeInsets.only(top: 50.0),
      padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 30.0),
      child: Text("MiauTranfer",
          style: TextStyle(
              color: Colors.black,
              fontSize: 30.0,
              fontWeight: FontWeight.bold)),
    );
  }
}

class AnimatedLogo extends AnimatedWidget {
  // Maneja los Tween estáticos debido a que estos no cambian.
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1.0);
  static final _sizeTween = Tween<double>(begin: 0.0, end: 150.0);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Opacity(
      opacity: _opacityTween.evaluate(animation),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        height: _sizeTween.evaluate(animation), // Aumenta la altura
        width: _sizeTween.evaluate(animation), // Aumenta el ancho
        child: FlutterLogo(),
      ),
    );
  }
}

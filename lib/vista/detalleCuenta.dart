import 'package:bancamovilapp/servicio/Global.dart';
import 'package:bancamovilapp/servicio/UsuarioService.dart';
import 'package:bancamovilapp/vista/cambioPassword.dart';
import 'package:bancamovilapp/vista/credito.dart';
import 'package:bancamovilapp/vista/detalleCredito.dart';
import 'package:bancamovilapp/vista/login.dart';
import 'package:bancamovilapp/vista/transacciones.dart';
import 'package:flutter/material.dart';

class MainPageState extends State<SecondRoute> {
  String title = '0';
  String expandedValue;
  String headerValue;
  bool isExpanded;
  double disponible = usuarioGlobal.saldoCuenta;
  double total = usuarioGlobal.saldoCuenta;
  Drawer _buildDrawer(context) {
    return new Drawer(
      child: new ListView(
        children: <Widget>[
          new DrawerHeader(
            child: new Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.network(
                    "https://img.icons8.com/cotton/128/000000/user-male--v1.png",
                    width: 80.0,
                    height: 80.0,
                    fit: BoxFit.cover,
                  ),
                  Text(
                    "Bienvenido " +
                        usuarioGlobal.nombre +
                        " " +
                        usuarioGlobal.apellido,
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            decoration: BoxDecoration(color: Colors.blue),
          ),
          ListTile(
              leading: new Icon(Icons.dashboard),
              title: Text("Resumen"),
              onTap: () {
                setState(() {
                  disponible = usuarioGlobal.saldoCuenta;
                  total = usuarioGlobal.saldoCuenta;
                });

                Navigator.pop(context);
              }),
          ListTile(
              leading: new Icon(Icons.monetization_on),
              title: Text("Creditos"),
              onTap: () {
                setState(() {
                  getCreditos();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => FifthRoute()));
                });
              }),
          ListTile(
            leading: new Icon(Icons.credit_card),
            title: Text("Transferencias"),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ThirdRoute()));
            },
          ),
          ListTile(
            leading: new Icon(Icons.settings),
            title: Text("Configuracion"),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => fourthRoute()));
            },
          ),
          ListTile(
              leading: new Icon(Icons.close),
              title: Text("Cerrar Sesion"),
              onTap: () {
                logueado = false;
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => LoginScreen()));
              }),
          Divider(
            color: Colors.black12,
            indent: 16.0,
          ),
          ListTile(
            title: Text("Acerca de"),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MiauTranfer"),
      ),
      drawer: _buildDrawer(context),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 100.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            ExpansionTile(
              title: Text(
                "Cuentas",
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              children: <Widget>[
                ExpansionTile(
                  title: Text('Cuenta de Ahorro'),
                  children: <Widget>[
                    ListTile(
                      title: Text('Numero de cuenta: ' +
                          "        " +
                          usuarioGlobal.numCuenta),
                    ),
                    ListTile(
                      title: Text('Disponible: ' +
                          "                       " +
                          disponible.toString()),
                    ),
                    ListTile(
                      title: Text('Saldo Total: ' +
                          "                      " +
                          total.toString()),
                    )
                  ],
                ),
                ExpansionTile(
                  title: Text("Cuenta Corriente"),
                  children: <Widget>[
                    ListTile(
                      title: Text(
                          'Numero de cuenta: ' + "        " + "Sin numero"),
                    ),
                    ListTile(
                      title: Text(
                          'Disponible: ' + "                       " + "0"),
                    ),
                    ListTile(
                      title: Text(
                          'Saldo Total: ' + "                      " + "0"),
                    )
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SecondRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainPageState();
  }
}

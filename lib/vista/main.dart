import 'package:flutter/material.dart';
import 'package:bancamovilapp/vista/login.dart';

void main() => runApp(new MaterialApp(
      home: LoginScreen(),
      title: 'Banca Movil',
    ));

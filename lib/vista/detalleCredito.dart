import 'package:bancamovilapp/modelo/credito.dart';
import 'package:bancamovilapp/servicio/Global.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatelessWidget {
  // Declara un campo que contenga el objeto Todo
  final List<Amortizacion> todo;
  int a = 0;
  // En el constructor, se requiere un objeto Todo
  DetailScreen({Key key, @required this.todo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Usa el objeto Todo para crear nuestra UI
    return Scaffold(
      appBar: AppBar(
        title: Text("Detalle de Credito"),
      ),
      body: DataTable(
          columns: const <DataColumn>[
            DataColumn(
              label: Text(
                "#",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                "Fecha de la Cuota",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                "Pago",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                "Estado",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                "Pendiente",
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
          ],
          rows: todo
              .map((element) => DataRow(cells: <DataCell>[
                    DataCell(Text((++a).toString())),
                    DataCell(Text(element.fechaCuota)),
                    DataCell(Text(element.cuotaAmortizacion.toString())),
                    DataCell(Text(element.estadoAmortizacion)),
                    DataCell(
                        Text(element.capitalPendienteAmortizacion.toString())),
                  ]))
              .toList()),
    );
  }
}

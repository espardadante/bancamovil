import 'package:bancamovilapp/servicio/Global.dart';
import 'package:bancamovilapp/servicio/UsuarioService.dart';
import 'package:bancamovilapp/vista/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainPasswordState extends State<fourthRoute> {
  GlobalKey<FormState> _key3 = GlobalKey();
  TextEditingController email = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Recuperar Contraseña'),
      ),
      body: new SingleChildScrollView(
        child: new Container(
          margin: new EdgeInsets.all(25.0),
          child: new Form(
            key: _key3,
            child: formUI(),
          ),
        ),
      ),
    );
  }

  Widget formUI() {
    return Column(
      children: <Widget>[
        formItemsDesign(TextFormField(
          controller: email,
          decoration: new InputDecoration(
            labelText: 'Contraseña nueva:',
          ),
        )),
        GestureDetector(
            onTap: () {
              save();
              print(reset);
              if (reset) {
                logueado = false;
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => LoginScreen()));
              } else {
                Navigator.pop(context);
              }
            },
            child: Container(
              margin: new EdgeInsets.all(30.0),
              alignment: Alignment.center,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                gradient: LinearGradient(colors: [
                  Color(0xFF0EDED2),
                  Color(0xFF03A0FE),
                ], begin: Alignment.topLeft, end: Alignment.bottomRight),
              ),
              child: Text("Guardar",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w500)),
              padding: EdgeInsets.only(top: 10, bottom: 10),
            )),
        Text(
            "Si su correo es correcto se cerrara la Sesion para que ingrese con su nueva contrasenia"),
      ],
    );
  }

  save() {
    resetPaswword(email.text);
  }
}

formItemsDesign(item) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 3),
    child: Card(child: ListTile(title: item)),
  );
}

class fourthRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainPasswordState();
  }
}
